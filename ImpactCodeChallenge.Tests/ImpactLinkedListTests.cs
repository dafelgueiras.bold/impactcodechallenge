﻿using Xunit;

using ImpactCodeChallenge.Lib;

namespace ImpactCodeChallenge.Tests
{
    public class ImpactLinkedListTests
    {
        [Fact]
        public void LinkedListTest()
        {
            var impactLinkedList = new ImpactLinkedList<string>();

            impactLinkedList.Add("One");
            impactLinkedList.Add("Two");
            impactLinkedList.Add("Three");
            impactLinkedList.Add("Four");
            impactLinkedList.Add("Five");

            Assert.True(impactLinkedList.GetCurrent().DataItem == "Five" && impactLinkedList.GetCurrent().Next == null);
            Assert.True(impactLinkedList.GetCurrent().Prev.DataItem == "Four" && impactLinkedList.GetCurrent().Prev.Next.DataItem == "Five");
            Assert.True(impactLinkedList.GetCurrent().Prev.Prev.DataItem == "Three" && impactLinkedList.GetCurrent().Prev.Prev.Next.DataItem == "Four");
            Assert.True(impactLinkedList.GetCurrent().Prev.Prev.Prev.DataItem == "Two" && impactLinkedList.GetCurrent().Prev.Prev.Prev.Next.DataItem == "Three");
            Assert.True(impactLinkedList.GetCurrent().Prev.Prev.Prev.Prev.DataItem == "One" && impactLinkedList.GetCurrent().Prev.Prev.Prev.Prev.Next.DataItem == "Two");
            Assert.True(impactLinkedList.GetCurrent().Prev.Prev.Prev.Prev.Prev == null);

            impactLinkedList.Remove();

            Assert.True(impactLinkedList.GetCurrent().DataItem == "Four" && impactLinkedList.GetCurrent().Next == null);
        }
    }
}
