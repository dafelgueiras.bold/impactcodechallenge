using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Xunit;

using ImpactCodeChallenge.Models;

namespace ImpactCodeChallenge.Tests
{
    public class NewsControllerTests
    {
        private readonly TestServer WebServer;
        private readonly HttpClient WebClient;

        public NewsControllerTests()
        {
            WebServer = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            WebClient = WebServer.CreateClient();
        }

        [Fact]
        public async void GetRssNewsTest()
        {
            var feed = await GetRssNewsFeed();
        }

        [Fact]
        public async void GetRssNewsFeedResultTest()
        {
            var feed = await GetRssNewsFeed();
            Type feedType = typeof(Feed);

            Assert.IsType(feedType, feed);
            Assert.True(String.IsNullOrEmpty(feed.Title) == false);
            Assert.True(String.IsNullOrEmpty(feed.Image.Url) == false);
            Assert.True(feed.Entries.Length > 0);

            feed.Entries.ToList().ForEach(e => {
                Assert.True(String.IsNullOrEmpty(e.Title) == false);
                Assert.True(String.IsNullOrEmpty(e.Link) == false);
                Assert.True(String.IsNullOrEmpty(e.PubDate) == false);
            });


        }

        private async Task<Feed> GetRssNewsFeed()
        {
            var response = await WebClient.GetAsync("/api/News/GetRssNews?feed=http://rss.cnn.com/rss/edition.rss");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            return JsonConvert.DeserializeObject<Feed>(responseString);
        }
    }
}
