﻿using Newtonsoft.Json;

namespace ImpactCodeChallenge.Models
{
    public class FeedImg
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

}
