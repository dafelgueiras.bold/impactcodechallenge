﻿using System;
using System.Linq;
using Newtonsoft.Json;
using ImpactCodeChallenge.Helpers;

namespace ImpactCodeChallenge.Models
{
    public class Feed
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("authorsToday")]
        public int AuthorsToday { get => ParseAuthorsToday(); }

        [JsonProperty("desc")]
        public string Description { get; set; }

        [JsonProperty("img")]
        public FeedImg Image { get; set; }

        [JsonProperty("entries")]
        public FeedEntry[] Entries { get; set; }

        private int ParseAuthorsToday()
        {
            var last24HoursEntries = this.Entries.ToList().Where(e => Converters.ConvertAsImpactDateTime(DateTime.Now).Subtract(DateTime.Parse(e.PubDate)).TotalHours <= 24);

            if(last24HoursEntries.Count() != 0)
            {
                return last24HoursEntries.Aggregate(0, (a, b) => a + b.Authors.Length);
            }

            return 0;
        }
    }

}
