﻿using Newtonsoft.Json;

using ImpactCodeChallenge.Helpers;

namespace ImpactCodeChallenge.Models
{
    public class FeedEntry
    {
        private string pubDate;

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("pubDate")]
        public string PubDate 
        {
            get => Converters.ConvertAsImpactDateTime(pubDate).ToString("dd-MM-yyyy HH:mm:ss");
            set => pubDate = value;
        }

        [JsonProperty("authors")]
        public string[] Authors { get; set; }
    }

}
