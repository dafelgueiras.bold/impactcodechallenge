﻿using System;
using System.Linq;
using System.Xml;
using System.ServiceModel.Syndication;

using ImpactCodeChallenge.Models;

namespace ImpactCodeChallenge.Services
{
    public static class FeedService
    {
        public static Feed Fetch(string url)
        {
            using (var rssReader = XmlReader.Create(url))
            {
                SyndicationFeed sFeed = SyndicationFeed.Load(rssReader);
                var feed = new Feed
                {
                    Title = sFeed.Title?.Text,
                    Description = sFeed.Description?.Text,
                    Image = new FeedImg 
                    {
                        Title = sFeed.Title?.Text,
                        Url = sFeed.ImageUrl?.AbsoluteUri
                    },
                    Entries = sFeed.Items?.ToList().Select(i => {
                        return new FeedEntry
                        {
                            Title = i.Title?.Text,
                            Link = i.Links?.FirstOrDefault().Uri.AbsoluteUri,
                            PubDate = i.PublishDate != null ? i.PublishDate.DateTime.ToString() : null,
                            Authors = i.Authors.Count() > 0 ? i.Authors?.ToList().Where(a => String.IsNullOrEmpty(a.Name) == false).Select(a => a.Name).ToArray() : Array.Empty<string>()
                        };
                    }).OrderByDescending(e => e.PubDate).ToArray()
                };

                return feed;
            }
        }
    }
}
