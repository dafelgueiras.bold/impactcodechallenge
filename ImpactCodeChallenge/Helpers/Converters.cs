﻿using System;

namespace ImpactCodeChallenge.Helpers
{
    public static class Converters
    {
        public static DateTime ConvertAsImpactDateTime(DateTime datetime)
        {
            return GetImpactDateTime(datetime);
        }
        public static DateTime ConvertAsImpactDateTime(string datetime)
        {
            return GetImpactDateTime(DateTime.Parse(datetime));
        }

        //Converts DateTime to GMT+2
        private static DateTime GetImpactDateTime(DateTime datetime)
        {
            var impactTimeZoneId = "+2";
            var impactTimeZoneDisplayName = "(GMT+02:00) Impact/GMT+2";
            var impactTimeZoneStandardDisplayName = "GMT+2";
            var impactTimeZone = TimeZoneInfo.CreateCustomTimeZone(impactTimeZoneId, new TimeSpan(02, 00, 00), impactTimeZoneDisplayName, impactTimeZoneStandardDisplayName);

            var newDateTime = TimeZoneInfo.ConvertTime(datetime, TimeZoneInfo.Local, impactTimeZone);

            return newDateTime;
        }
    }
}
