﻿using Microsoft.AspNetCore.Mvc;

namespace ImpactCodeChallenge.Controllers
{
    [Route("/")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        [HttpGet]
        public RedirectResult Get()
        {
            return Redirect("~/swagger/");
        }
    }
}
