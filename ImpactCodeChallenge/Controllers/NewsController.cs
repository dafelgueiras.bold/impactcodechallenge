﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using ImpactCodeChallenge.Services;

namespace ImpactCodeChallenge.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : Controller
    {
        /// <summary>
        /// Gets Rss Feed News
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/News/GetRssNews?feed=http://rss.cnn.com/rss/edition.rss
        ///
        /// </remarks>
        /// <param name="feed"></param>
        /// <returns>A Feed Model Entry from given Rss Feed Provider</returns>
        /// <response code="200">Returns the Feed Model Entry</response>    
        /// <response code="400">Returns the validation errors.</response>    
        /// <response code="500">Returns an Internal Server error and the Rss Feed Provider error message if not available.</response>
        [HttpGet("GetRssNews")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public JsonResult GetRssNews([FromQuery, BindRequired] string feed)
        {
            try
            {
                return Json(FeedService.Fetch(feed));
            }
            catch (System.Net.WebException exception)
            {
                var result = Json($"Error while fetching results from RssFeed {feed} ({exception.Message}).");
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }
    }
}
