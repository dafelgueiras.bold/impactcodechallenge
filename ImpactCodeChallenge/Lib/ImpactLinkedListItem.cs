﻿namespace ImpactCodeChallenge.Lib
{
    public class ImpactLinkedListItem<T>
    {
        public ImpactLinkedListItem<T> Prev;
        public ImpactLinkedListItem<T> Next;
        public T DataItem;
    }

}
