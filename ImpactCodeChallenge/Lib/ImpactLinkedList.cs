﻿namespace ImpactCodeChallenge.Lib
{
    public class ImpactLinkedList<T>
    {
        private ImpactLinkedListItem<T> Current;

        //Get Current Linked Item
        public ImpactLinkedListItem<T> GetCurrent()
        {
            return Current;
        }

        //Add Linked Item to Last
        public void Add(T dataItem)
        {
            ImpactLinkedListItem<T> newItem = GetNewItem(dataItem);

            if (Current != null)
            {
                var _prev = Current;
                _prev.Next = newItem;
                newItem.Prev = _prev;
            }
            Current = newItem;
        }

        //Remove Last Added Linked Item
        public void Remove()
        {
            Current = Current.Prev;
            Current.Next = null;
        }


        private ImpactLinkedListItem<T> GetNewItem(T data)
        {
            return new ImpactLinkedListItem<T>
            {
                DataItem = data,
                Prev = null,
                Next = null
            };
        }
    }
}
